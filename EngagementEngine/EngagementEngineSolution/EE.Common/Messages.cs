﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EE.Common
{
    public class EngagementMessage
    {


        public string Id { get; set; }

        public string Context { get;   set; }
        public string ContentId { get;   set; }
        public string EngagementType { get; set; }
        public string UserName { get;   set; }
        public string LanguageCode { get;   set; }
        public string Gender { get;   set; }
        public string AgeGroup { get;   set; }
        //public string Demographics { get; set; }

        public string DateCreated { get;   set; }
        public string DateRecieved { get;   set; }
        public string Password { get; set; }

       

        public EngagementMessage()
        {
        }

        public  EngagementMessage(string s)
        {
            string[] sa = s.Split(",".ToCharArray());
          
            this.Context = sa[0];
            this.ContentId = sa[1];
            //this.EngagementType = (EngagementType)Convert.ToInt16(sa[2]);
            this.EngagementType = sa[2];
            this.UserName = sa[3];
            this.LanguageCode = sa[4];
            this.Gender = sa[5];
            this.AgeGroup = sa[6];
            this.DateCreated = sa[7];

            
        }

      
        
    }

     
}