﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace EE.Common
{
    public static class Constants
    {
        public const string QueueName_EngagementJob = "engagementjobqueue";
        public const string QueueName_StatsJob = "statsjobqueue";
        public const string QueueName_DemograhpicsJob = "demograhpicsjobqueue";
        public const string QueueName_BuddyJob = "buddyjobqueue";
        public static string QueueName_BuddyListener = "buddylistenerjobqueue";
        public const string QueueName_BuddySuggestions = "buddysuggestionsjobqueue";
        //-----
        public const string TableName_Engagement = "contentengagement";
        public const string TableName_UserEngagement = "userengagement";
        public const string TableName_DemographicEngagement = "engagementdemographics";
        public const string TableName_PopularDemographicEngagement = "popularengagementdemographics";
        public const string TableName_BuddyList = "buddies";
        public const string TableName_BuddyListeners = "buddylisteners";
        public const string TableName_BuddySuggestions = "buddysuggestions";
        //-----
        public static string EngagementType_Like = "like";
        public static string EngagementType_View = "view";
        public static string EngagementType_Reshare = "reshare";
        public static string EngagementType_Comment = "comment";
        public static string EngagementType_Share = "share";
        //-----
        public static string ContextType_UGC = "ugc";
        public static string ContextType_Music = "music";
        public static string ContextType_Following = "following";
        //-----
        public static string Gender_Male = "m";
        public static string Gender_Female = "f";
        public static string Gender_Other = "o";
        public static string Gender_Unknown = "u";


    }
}