﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EE.Common
{
  

    public class Buddy
    {
        public string UserName { get; set; }

        public string Context { get; set; }
        public string BuddyName { get; set; }
        public string Percentage { get; set; }
    
      
      


    }


    public class UsageReport
    {
        public string Id { get; set; }
        public string Region { get; set; }
        public string Gender { get; set; }
        public string AgeGroup { get; set; }
        public string UserName { get; set; }
        public string Channel { get; set; }
    }

    public class Answer
    {
        public string ProfileCode { get; set; }
        public List<string> ContentList { get; set; }
        public List<string> ChannelList { get; set; }
        //public string Count { get; set; }
        //public string UserName { get; set; }

    }
}