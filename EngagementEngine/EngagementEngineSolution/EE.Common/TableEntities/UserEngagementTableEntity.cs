﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE.Common
{
   
        public class UserEngagementTableEntity:TableEntity
        {
            //public string PartitionKey { get; set; }
            //public string RowKey { get; set; }
            public UserEngagementTableEntity()
            {
                
            }

            public UserEngagementTableEntity(EngagementMessage e)
            {
                this.PartitionKey = e.UserName;
                this.RowKey = e.Context + "," + e.ContentId + "," + e.EngagementType;

                this.DateCreated = e.DateCreated;
            }


            public string DateCreated { get; set; }

        }



    }
 