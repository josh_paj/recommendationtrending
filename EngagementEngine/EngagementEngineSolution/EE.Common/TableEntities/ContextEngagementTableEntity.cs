﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE.Common
{
   
        public class ContextEngagementTableEntity
        {
            public string PartitionKey { get; set; }
            public string RowKey { get; set; }
            public ContextEngagementTableEntity()
            {

            }

            public ContextEngagementTableEntity(EngagementMessage engagement)
            {
                string pk = engagement.Context + "," + engagement.ContentId;
                this.PartitionKey = pk;
                this.RowKey = engagement.UserName + "," + engagement.EngagementType;

                this.DateCreated = engagement.DateCreated;
            }
            //decide later if we want this incremented or we just add up the rows
            public int Count { get; set; }
            public string DateCreated { get; set; }
        }



    }
 