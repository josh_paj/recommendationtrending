﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE.Common
{
   
        public class DemographicTableEntity:TableEntity
        {
            //public string PartitionKey { get; set; }
            //public string RowKey { get; set; }
            public DemographicTableEntity()
            {
                
            }

            public DemographicTableEntity(EngagementMessage e)
            {
                this.PartitionKey = e.Context + "," + e.ContentId + "," + e.EngagementType;
                this.RowKey = e.LanguageCode + "," + e.Gender + "," + e.AgeGroup;

                this.DateCreated = e.DateCreated;
            }
            public int Count { get; set; }
            public string DateCreated { get; set; }

        }



    }
 