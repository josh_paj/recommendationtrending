﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using EE.Common;

namespace EE.LogWebJob
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        public static void ProcessQueueMessage([QueueTrigger(Constants.QueueName_StatsJob)] EngagementMessage message,
            [Table(Constants.TableName_Engagement)] ICollector<ContextEngagementTableEntity> tableBinding,
            [Table(Constants.TableName_UserEngagement)] ICollector<UserEngagementTableEntity> tableBinding2,
            [Queue(EE.Common.Constants.QueueName_DemograhpicsJob)] out EngagementMessage outputQueueMessage2,
            TextWriter log)
        {

            //tableBinding.Add(new ContextEngagementTableEntity(message));
            //tableBinding2.Add(new UserEngagementTableEntity(message));

            outputQueueMessage2 = message;
            ////////////[Table(Constants.TableName_UserEngagement)] ICollector<UserEngagementTableEntity> tableBinding2,

            log.WriteLine(message);
        }

        //[NoAutomaticTrigger]
        //public static void IngressDemo(
        //    [Table("Ingress")] ICollector<Person> tableBinding)
        //{
        //    for (int i = 0; i < 100000; i++)
        //    {
        //        tableBinding.Add(
        //            new Person()
        //            {
        //                PartitionKey = "Test",
        //                RowKey = i.ToString(),
        //                Name = "Name"
        //            }
        //            );
        //    }
        //}
    }
}
