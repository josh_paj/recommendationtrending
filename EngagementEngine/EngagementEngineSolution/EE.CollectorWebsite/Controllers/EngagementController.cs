﻿using EE.Common;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace RSWebCollector.Controllers
{
    public class RepositoryBase
    {
        public CloudStorageAccount StorageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);


    }
    public class EngagementController : ApiController
    {
        static readonly IEngagementRepository repository = new EngagementRepository();

        public HttpResponseMessage Post(EngagementMessage item)
        {
            //if (password == "thepasswordis123456789")
            if (item.Password == "password")
            {
                item = repository.Add(item);
                var response = Request.CreateResponse<EngagementMessage>(HttpStatusCode.Created, item);
                return response;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.BadRequest);
                return response;
            }
        }

        public interface IEngagementRepository
        {
            EngagementMessage Add(EngagementMessage item);
        }

        public class EngagementRepository : RepositoryBase, IEngagementRepository
        {
            public EngagementMessage Add(EngagementMessage item)
            {
                CloudQueueClient queueClient = StorageAccount.CreateCloudQueueClient();

                CloudQueue queue = queueClient.GetQueueReference(Constants.QueueName_EngagementJob);
                queue.CreateIfNotExists();
                //----------

                CloudQueueMessage message = new CloudQueueMessage(JsonConvert.SerializeObject(item));
                

                //string messageContent = item.Context + "," + item.ContentId + "," + item.EngagementType + "," + item.UserName + "," + item.LanguageCode + "," + item.Gender + "," + item.AgeGroup + "," + item.DateCreated;

                //CloudQueueMessage message = new CloudQueueMessage(messageContent);
                //----------
                queue.AddMessage(message);

                return item;
            }

        }
    }
}