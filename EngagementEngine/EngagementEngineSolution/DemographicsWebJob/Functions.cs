﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using EE.Common;

namespace DemographicsWebJob
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        public static void ProcessQueueMessage(
            [QueueTrigger(EE.Common.Constants.QueueName_DemograhpicsJob)] EngagementMessage message,
            //[Table(Constants.TableName_DemographicEngagement, "{PartitionKey}", "{RowKey}")]  DemographicTableEntity demo,
            [Table(Constants.TableName_PopularDemographicEngagement)] IQueryable<DemographicTableEntity> tableBinding,
                [Table(Constants.TableName_PopularDemographicEngagement)] ICollector<DemographicTableEntity> tableBinding2,

            TextWriter log)
        {
         
            string pk = message.Context + "," + message.LanguageCode + "," + message.Gender + "," + message.AgeGroup + "," + message.EngagementType;
            string rk = message.ContentId;

            var query = from p in tableBinding where (p.PartitionKey == pk && p.RowKey==rk) select p;

            if(query.Count()>0)
            {
             
            }
            else
            {
                tableBinding2.Add(new DemographicTableEntity(message));
            }

          

            log.WriteLine(message);
        }


        public class OtherStuff
        {
            public string Count { get; set; }
            public string DateCreated { get; set; }
            //public TimeSpan Duration { get; set; }
            //public string Value { get; set; }
        }

        //public static void TableDict([Table("mytable")] IDictionary<Tuple<string, string>, OtherStuff> dict)
        //{
        //    // Use IDictionary interface to access an azure table.
        //    var partRowKey = Tuple.Create("PartitionKeyValue", "RowKeyValue");
        //    OtherStuff val;
        //    bool found = dict.TryGetValue(partRowKey, out val);

        //    OtherStuff val2 = dict[partRowKey]; // lookup via indexer

        //    // another write exmaple
        //    dict[partRowKey] = new OtherStuff { Value = "fall", Fruit = Fruit.Apple, Duration = TimeSpan.FromMinutes(5) };
        //}
    }
}
