﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using EE.Common;

namespace EE.SubscribersWebJob
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        //public static void ProcessQueueMessage([QueueTrigger("queue")] string message, TextWriter log)
        //{
        //    log.WriteLine(message);
        //}

        public static void ProcessQueueMessage([QueueTrigger(EE.Common.Constants.QueueName_EngagementJob)] EngagementMessage message,
             [Queue(EE.Common.Constants.QueueName_StatsJob)] out EngagementMessage outputQueueMessage,
              //[Queue(EE.Common.Constants.QueueName_DemograhpicsJob)] out EngagementMessage outputQueueMessage2,
              [Queue(EE.Common.Constants.QueueName_BuddySuggestions)] out EngagementMessage outputQueueMessage3,
              [Queue(EE.Common.Constants.QueueName_BuddyJob)] out EngagementMessage outputQueueMessage4, 
            TextWriter log)
        {
            outputQueueMessage = message;
            //outputQueueMessage2 = message;
            outputQueueMessage3 = message;
            outputQueueMessage4 = message;

            log.WriteLine(message);
        }
    }
}
